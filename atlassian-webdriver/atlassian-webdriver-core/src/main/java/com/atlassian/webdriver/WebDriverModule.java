package com.atlassian.webdriver;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.pageobjects.browser.BrowserAware;
import com.atlassian.webdriver.pageobjects.PageFactoryPostInjectionProcessor;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.google.inject.Binder;
import com.google.inject.Module;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;

/**
 * Guice module providing Atlassian WebDriver components.
 *
 * @since 2.0
 */
public class WebDriverModule implements Module
{
    private final TestedProduct<? extends WebDriverTester> testedProduct;

    public WebDriverModule(TestedProduct<? extends WebDriverTester> testedProduct)
    {
        this.testedProduct = testedProduct;
    }

    public void configure(Binder binder)
    {
        WebDriver driver = testedProduct.getTester().getDriver();

        binder.bind(WebDriverContext.class).toInstance(testedProduct.getTester());
        binder.bind(BrowserAware.class).toInstance(testedProduct.getTester());
        binder.bind(Browser.class).toInstance(testedProduct.getTester().getBrowser());
        binder.bind(WebDriver.class).toInstance(driver);
        binder.bind(SearchContext.class).toInstance(driver);
        binder.bind(JavascriptExecutor.class).toInstance((JavascriptExecutor) driver);
        binder.bind(PageFactoryPostInjectionProcessor.class);
    }
}
